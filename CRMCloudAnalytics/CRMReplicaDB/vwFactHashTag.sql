﻿CREATE VIEW [dbo].[vwFactHashTag]
	AS 

SELECT htag.[Id]										AS HashtagKey
	  ,inc2.CaseKey										AS CaseKey
	  ,inc2.AccountKey									AS AccountKey
	  ,inc2.ContactKey									AS ContactKey
	  ,inc2.RepKey										AS RepKey
	  ,inc2.TeamKey										AS TeamKey
	  ,inc2.CreatedOnDateKey
	  ,inc2.CreatedOnHour
	  ,inc2.CreatedOnMinute
	  ,inc2.CreatedOnSecond
FROM [dbo].[trek_hashtag] AS htag

	INNER JOIN (SELECT 
					 inc.[Id]									AS CaseKey
					,inc.trek_hashtagid								AS HashtagKey
					,CASE WHEN  inc.customerid_entitytype = 'account'
						THEN acc.id					
						ELSE CAST(0x0 AS UNIQUEIDENTIFIER)
						END											AS AccountKey
					,CASE WHEN  inc.customerid_entitytype = 'contact'
						THEN con.id				
						ELSE CAST(0x0 AS UNIQUEIDENTIFIER)
						END											AS ContactKey
					,CASE WHEN  inc.ownerid_entitytype = 'systemuser'
						THEN rep.id			
						ELSE CAST(0x0 AS UNIQUEIDENTIFIER)
						END											AS RepKey
					,CASE WHEN  inc.ownerid_entitytype = 'team'
						THEN team.id		
						ELSE CAST(0x0 AS UNIQUEIDENTIFIER)
						END											AS TeamKey
					  ,CAST(inc.[CreatedOn] AS DATE)	 	AS CreatedOnDateKey
					  ,DATEPART(HOUR,inc.CreatedOn) AS CreatedOnHour
					  ,DATEPART(MINUTE,inc.createdon) AS CreatedOnMinute
					  ,DATEPART(SECOND,inc.createdon) AS CreatedOnSecond
				FROM [dbo].[incident] inc
				LEFT OUTER JOIN dbo.account AS acc
				ON inc.customerid = acc.id
 				LEFT OUTER JOIN dbo.contact AS con
				ON inc.customerid = con.id
 				LEFT OUTER JOIN dbo.systemuser AS rep
				ON inc.ownerid = rep.id
				LEFT OUTER JOIN dbo.team AS team
				ON inc.ownerid = team.id) AS inc2
	ON  inc2.HashtagKey = htag.id