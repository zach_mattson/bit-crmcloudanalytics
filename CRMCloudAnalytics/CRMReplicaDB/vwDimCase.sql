﻿USE [CRM_TrekBikes]
GO

/****** Object:  View [dbo].[vwDimCase]    Script Date: 2/5/2018 4:26:12 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER VIEW [dbo].[vwDimCase]
	AS 
	 
  --DECLARE @LangCode INT = @LangCode = '1033'		-- Language Code, i guess this is for the us english from some table somewhere
  --DECLARE @EntityName NVARCHAR(30) = @EntityName = 'incident'	-- Entity Filter, a case is an 'incident')


  SELECT  inc.[incidentid]							AS [CaseId]
		 ,inc.[title]								AS [CaseTitle]
		 --,inc.[description]							AS [CaseDescription]
		 ,inc.[ticketnumber]						AS [CaseTicketNo]
		 ,orig.[LocalizedLabel]						AS [CaseOrigin]
		 ,titl.[LocalizedLabel]						AS [CaseType]
		 ,topc.[LocalizedLabel]						AS [CaseTopic]
		 ,stte.[LocalizedLabel]						AS [CaseState]
		 ,stts.[LocalizedLabel]						AS [CaseStatus]
		 ,ucrt.[Name]								AS [CaseCreatedBy]
		 ,umod.[Name]								AS [CaseModifiedBy]
		 
--		 ,uteam.[Name]								AS [CaseTeam]
		 ,cust.[CustomerName]						AS [CaseCustomerName]
		 ,inc.[customerid_entitytype]				AS [CaseCustomerType]
		 ,inc.[new_context]							AS [CaseLocale]
		 ,inc.trek_hashtagid						AS [CaseHashtagKey]
		 
		 ,own.[Name]								AS [CurrentOwner]
		 ,own.[type]								AS [CurrentOwnerType]
		 ,ISNULL(pown.[Name],'None')				AS [PreviousOwner]
		 ,ISNULL(pown.[type],'None')				AS [PreviousOwnerType]

		 

  FROM [dbo].[incident] AS inc			  						--Incident (Case) table
	LEFT OUTER JOIN dbo.vwGlobalOptionSet AS orig								-- Pull In Case Origin
		ON inc.caseorigincode = orig.[option]
		AND orig.OptionSetName = 'caseorigincode'

	LEFT OUTER JOIN dbo.vwOptionSet  AS titl									--Pull In Title
		ON inc.casetypecode = titl.[option]
		AND titl.EntityName = 'incident'
		AND titl.OptionSetName = 'casetypecode'

	LEFT OUTER JOIN dbo.vwGlobalOptionSet AS topc									--Pull In Topic
		ON inc.casetypecode = topc.[option]		
		AND topc.optionsetname = 'new_topic'

	LEFT OUTER JOIN [dbo].[StateMetadata] AS stte					--Pull In State
		ON inc.statecode = stte.[State]
		AND stte.EntityName = 'incident'
		AND stte.LocalizedLabelLanguageCode = 1033

	LEFT OUTER JOIN [dbo].[StatusMetadata] AS stts					--Pull In Status
		ON inc.statecode = stts.[State]
		AND inc.statuscode = stts.[Status]
		and stts.EntityName = 'incident'
		AND stts.LocalizedLabelLanguageCode = 1033

	LEFT OUTER JOIN [dbo].[vwOwner] AS ucrt									-- Created By
		ON inc.createdby = ucrt.GUID
		AND inc.createdby_entitytype = ucrt.[type] --systemuser

	LEFT OUTER JOIN [dbo].[vwOwner] AS umod									-- Modified By
		ON inc.modifiedby = umod.GUID
		AND inc.modifiedby_entitytype = ucrt.[type] --systemuser

	LEFT OUTER JOIN [dbo].[vwOwner] AS own									-- Owner and OwnerType
		ON inc.ownerid = own.GUID
		AND inc.ownerid_entitytype = own.[type] --systemuser or team

	LEFT OUTER JOIN [dbo].[vwOwner] AS pown									-- Previous Owner and Prev Owner Type
		ON inc.[trek_previouscaseteamowner] = pown.GUID
		AND inc.[trek_previouscaseteamowner_entitytype] = pown.[type] --systemuser or team
		 
	LEFT OUTER JOIN dbo.vwCustomer AS cust								-- Customer (Contact or Account)
		ON inc.[customerid] = cust.CustomerGUID
		AND inc.[customerid_entitytype] = cust.CustomerType
WHERE
 inc.createdOn > '2017-01-01' -- baseline for the info
 --ORDER BY inc.createdOn DESC
		/*
		
		Case - IsTeamAtCreation
Case - IsUserAtCreation
Case Assignment - Team to User
Case Assignment - Team to Team
Case Assignment - User to Team
Case Assignment - User to User

When a case is initially created, it is owned by a team or a user, but is never assigned. The business customer would like to be able to filter by this.
The DimCase view can be filtered using:

 WHERE CaseOwnerType = 'team' AND CasePrvOwnerType IS NULL
and

 WHERE CaseOwnerType = 'systemuser' AND CasePrvOwnerType IS NULL
to get these results.

After a case is created, it can be assigned to a user or a team. The business customer would like to be able to filter by this.
The Dimcase view can be filtered using (respectively):

 WHERE CaseOwnerType = 'systemuser' AND CasePrvOwnerType = 'team'
 WHERE CaseOwnerType = 'team' AND CasePrvOwnerType = 'team'
 WHERE CaseOwnerType = 'systemuser' AND CasePrvOwnerType = systemuser'
 WHERE CaseOwnerType = 'team' AND CasePrvOwnerType = 'systemuser'
Mock-ups
		
		*/

GO


