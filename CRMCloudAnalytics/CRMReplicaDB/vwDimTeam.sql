﻿CREATE VIEW [dbo].[vwDimTeam]
AS
SELECT team.[Id]					AS TeamGUID
      ,team.[name]					AS TeamName
	  --Territory Heirarchy
	  ,cont.po_continent			AS TeamContinent
	  ,coun.po_country				AS TeamCountry
	  ,regn.name					AS TeamRegion
	  ,Omgr.fullname			    AS TeamOSM_RegManager
  FROM [dbo].[team] AS team
		LEFT OUTER JOIN dbo.po_continent AS cont	--Continent
		ON team.po_continentid = cont.id

		LEFT OUTER JOIN dbo.po_country AS coun		--Country
		ON team.po_countryid = coun.id

		LEFT OUTER JOIN dbo.territory AS regn		--Region
		ON team.po_region = regn.Id

		LEFT OUTER JOIN dbo.systemuser AS OMgr		--OSM Reg manager
		ON team.po_osmregionalmanager = Omgr.Id



GO

