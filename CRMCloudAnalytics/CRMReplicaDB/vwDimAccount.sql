﻿CREATE VIEW [dbo].[vwDimAccount]
	AS 
SELECT  acct.[Id]				AS AccountGUID
      ,acct.[name]				AS AccountName
	  ,stts.[LocalizedLabel]	AS AccountStatus
	  ,acct.accountnumber		AS AccountNumber
	  ,pacct.name				AS AccountParentName
	  ,pacct.accountnumber		AS AccountParentNumber
  FROM [dbo].[account] AS acct
	 INNER JOIN [dbo].StatusMetadata AS stts
		ON acct.statuscode = stts.[status]
		AND [LocalizedLabelLanguageCode] = '1033'
		AND EntityName= 'account'

	 LEFT JOIN dbo.account AS pacct					--Bring in parent account name and number
		ON pacct.accountid = acct.parentaccountid