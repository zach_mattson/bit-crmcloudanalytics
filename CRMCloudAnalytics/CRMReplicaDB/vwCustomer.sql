﻿CREATE VIEW [dbo].[vwCustomer]
	AS SELECT [Id]					AS CustomerGUID
		,'account'				AS CustomerType
		,[name]					AS CustomerName
	FROM [dbo].[account]

	UNION ALL

	SELECT [Id]					AS CustomerGUID
		,'contact'				AS CustomerType
		,[fullname]				AS CustomerName
	FROM [dbo].[contact]
