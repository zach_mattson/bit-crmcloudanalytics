﻿CREATE VIEW [dbo].[vwDimRep]
	AS 

SELECT usr.[Id]					AS RepGUID
      ,usr.[title]					AS RepTitle
      ,usr.[fullname]				AS RepName
	  ,CASE	WHEN usr.isdisabled = 1 
			THEN 'Disabled'
			ELSE 'Active'
	  END						AS RepStatus
	  ,cont.po_continent		AS RepContinent
	  ,coun.po_country			AS RepCountry
	  ,slsa.name				AS RepSalesArea	
	  ,regn.name				AS RepRegion
	  ,team.name				AS RepPrimaryCustCareTeam
	  ,mgr.fullname				AS RepManager

  FROM [dbo].[systemuser] AS usr
	LEFT OUTER JOIN dbo.po_continent AS cont	-- Bring In Continent
		ON usr.po_continentid = cont.Id

	LEFT OUTER JOIN dbo.po_country AS coun	-- Bring In Country
		ON usr.po_countryid = coun.id

	LEFT OUTER JOIN dbo.team AS slsa -- Bring in Sales Area
		ON usr.po_SalesArea = slsa.Id
		AND usr.po_salesarea_entitytype = 'team'

	LEFT OUTER JOIN dbo.territory AS regn		--Bring in Region
		ON usr.territoryid = regn.id

	LEFT OUTER JOIN dbo.team AS team -- Primary Customer Care Team
		ON usr.trek_primarycustomercareteam = team.Id
		AND usr.trek_primarycustomercareteam_entitytype = 'team'

	LEFT OUTER JOIN dbo.systemuser AS mgr
		ON usr.[parentsystemuserid] = mgr.id
