﻿CREATE VIEW [dbo].[vwDimContact]
	AS 
SELECT   cont.[Id]					AS ContactGUID
		,cont.[fullname]			AS ContactName
		,stts.[LocalizedLabel]		AS ContactStatus
  FROM [dbo].[contact] AS cont
  	 INNER JOIN [dbo].StatusMetadata AS stts
		ON cont.statuscode = stts.[status]
		AND stts.[LocalizedLabelLanguageCode] = '1033'
		AND stts.EntityName= 'contact'