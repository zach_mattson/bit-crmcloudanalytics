﻿CREATE VIEW [dbo].[vwOwner]
	AS 
	SELECT [Id]					AS [GUID]
	  ,'systemuser'				AS [Type]
      ,[fullname]				AS [Name]
	FROM [dbo].[systemuser]
	UNION ALL
	SELECT [Id]					AS [GUID]
	  ,'team'					AS [Type]
      ,[name]					AS [Name]
	FROM [dbo].[team]
