﻿CREATE VIEW [dbo].[vwOptionSet]
	AS 
	SELECT [EntityName]
		,[OptionSetName]
		,[Option]
		,[IsUserLocalizedLabel]
		,[LocalizedLabelLanguageCode]
		,[LocalizedLabel]
  FROM [dbo].[OptionSetMetadata]
  WHERE LocalizedLabelLanguageCode = 1033
