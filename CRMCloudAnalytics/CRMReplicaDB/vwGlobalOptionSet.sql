﻿CREATE VIEW [dbo].[vwGlobalOptionSet]
	AS 
	SELECT [OptionSetName]
		,[Option]
		,[IsUserLocalizedLabel]
		,[LocalizedLabelLanguageCode]
		,[LocalizedLabel]
  FROM [dbo].[GlobalOptionSetMetadata]
  WHERE LocalizedLabelLanguageCode = 1033
