﻿CREATE VIEW [dbo].[vwFactCase]
	AS 

SELECT 
 inc.[Id]										AS CaseKey
	  ,ISNULL(inc.trek_hashtagid, CAST(0x0 AS UNIQUEIDENTIFIER))		AS HashtagKey
	  ,CASE WHEN  inc.customerid_entitytype = 'account'
			THEN acc.id					
			ELSE CAST(0x0 AS UNIQUEIDENTIFIER)
			END											AS AccountKey
	  ,CASE WHEN  inc.customerid_entitytype = 'contact'
			THEN con.id				
			ELSE CAST(0x0 AS UNIQUEIDENTIFIER)
			END											AS ContactKey
	  ,CASE WHEN  inc.ownerid_entitytype = 'systemuser'
			THEN rep.id			
			ELSE CAST(0x0 AS UNIQUEIDENTIFIER)
			END											AS RepKey
	  ,CASE WHEN  inc.ownerid_entitytype = 'team'
			THEN team.id		
			ELSE CAST(0x0 AS UNIQUEIDENTIFIER)
			END											AS TeamKey
	  --,[CreatedOn]									AS CreatedOnKey
      ,CAST(inc.[CreatedOn] AS DATE)	 	AS CreatedOnDateKey
	  ,DATEPART(HOUR,inc.CreatedOn) AS CreatedOnHour
	  ,DATEPART(MINUTE,inc.createdon) AS CreatedOnMinute
	  ,DATEPART(SECOND,inc.createdon) AS CreatedOnSecond
	  ,CAST('1'	AS BIT)									AS [CaseCount]
	  ,ABS(DATEDIFF(MINUTE,inc.trek_assignedtorepon,inc.[CreatedOn]))  AS QueueTimeMins
	  ,ABS(DATEDIFF(MINUTE,res.irCreatedOn,inc.trek_assignedtorepon)) AS OpenTimeMins
	  ,inc.[CreatedOn] AS CreatedOnDateTimeKey
	  ,inc.trek_assignedtorepon
	  ,(ABS(DATEDIFF(MINUTE,inc.trek_assignedtorepon,inc.[CreatedOn])) + ABS(DATEDIFF(MINUTE,res.irCreatedOn,inc.trek_assignedtorepon)))
		 AS TotalTimeMins
		 
  FROM [dbo].[incident] inc
	LEFT OUTER JOIN dbo.account AS acc
		ON inc.customerid = acc.id
 	LEFT OUTER JOIN dbo.contact AS con
		ON inc.customerid = con.id
 	LEFT OUTER JOIN dbo.systemuser AS rep
		ON inc.ownerid = rep.id
	LEFT OUTER JOIN dbo.team AS team
		ON inc.ownerid = team.id

	LEFT OUTER JOIN (SELECT  [Id] AS irID,[createdon]	AS irCreatedOn	
						,ROW_NUMBER() OVER (PARTITION BY ID ORDER BY createdon ASC) AS irSort
					 FROM [dbo].[incidentresolution]
					) AS res
	    ON inc.[Id] = res.irID
		AND res.irSort = 1  -- minimum of date/time AS res
WHERE	inc.createdon >= DATEADD(yy, DATEDIFF(yy, 0, DATEADD(YEAR,-1,GETDATE())), 0)
	  
/*
Notes: Not sure how to best handle these time calculations:
 Queue time should be (in dbo.incident): CreatedOn - DateAssignedToRep
Open Times is (in dbo.incidentresolution, dbo.incident): DateAssignedToRep - 
(the createdon date of the first occurence of a resolution from dbo.incidentresolution table)

In the cube, FactCase will contain the following measures:

# Count of Cases
# QueueTime
# QueueTime - AvgCREATE VIEW [dbo].[vwFactCase]
	AS 

SELECT 
 inc.[Id]										AS CaseKey
	  ,ISNULL(inc.trek_hashtagid, CAST(0x0 AS UNIQUEIDENTIFIER))		AS HashtagKey
	  ,CASE WHEN  inc.customerid_entitytype = 'account'
			THEN acc.id					
			ELSE CAST(0x0 AS UNIQUEIDENTIFIER)
			END											AS AccountKey
	  ,CASE WHEN  inc.customerid_entitytype = 'contact'
			THEN con.id				
			ELSE CAST(0x0 AS UNIQUEIDENTIFIER)
			END											AS ContactKey
	  ,CASE WHEN  inc.ownerid_entitytype = 'systemuser'
			THEN rep.id			
			ELSE CAST(0x0 AS UNIQUEIDENTIFIER)
			END											AS RepKey
	  ,CASE WHEN  inc.ownerid_entitytype = 'team'
			THEN team.id		
			ELSE CAST(0x0 AS UNIQUEIDENTIFIER)
			END											AS TeamKey
	  --,[CreatedOn]									AS CreatedOnKey
      ,CAST(inc.[CreatedOn] AS DATE)	 	AS CreatedOnDateKey
	  ,DATEPART(HOUR,inc.CreatedOn) AS CreatedOnHour
	  ,DATEPART(MINUTE,inc.createdon) AS CreatedOnMinute
	  ,DATEPART(SECOND,inc.createdon) AS CreatedOnSecond
	  --,CAST('1'	AS BIT)									AS [CaseCount]
	  ,ABS(DATEDIFF(MINUTE,inc.trek_assignedtorepon,inc.[CreatedOn]))  AS QueueTimeMins
	  ,ABS(DATEDIFF(MINUTE,res.irCreatedOn,inc.trek_assignedtorepon)) AS OpenTimeMins
	  ,inc.[CreatedOn] AS CreatedOnDateTimeKey
	  ,inc.trek_assignedtorepon
	  
  FROM [dbo].[incident] inc
	LEFT OUTER JOIN dbo.account AS acc
		ON inc.customerid = acc.id
 	LEFT OUTER JOIN dbo.contact AS con
		ON inc.customerid = con.id
 	LEFT OUTER JOIN dbo.systemuser AS rep
		ON inc.ownerid = rep.id
	LEFT OUTER JOIN dbo.team AS team
		ON inc.ownerid = team.id

	LEFT OUTER JOIN (SELECT  [Id] AS irID,[createdon]	AS irCreatedOn	
						,ROW_NUMBER() OVER (PARTITION BY ID ORDER BY createdon ASC) AS irSort
					 FROM [dbo].[incidentresolution]
					) AS res
	    ON inc.[Id] = res.irID
		AND res.irSort = 1  -- minimum of date/time AS res
WHERE	inc.createdon >= DATEADD(yy, DATEDIFF(yy, 0, DATEADD(YEAR,-1,GETDATE())), 0)
	  
/*
Notes: Not sure how to best handle these time calculations:
 Queue time should be (in dbo.incident): CreatedOn - DateAssignedToRep
Open Times is (in dbo.incidentresolution, dbo.incident): DateAssignedToRep - 
(the createdon date of the first occurence of a resolution from dbo.incidentresolution table)

In the cube, FactCase will contain the following measures:

# Count of Cases
# QueueTime
# QueueTime - Avg
# OpenTime
# OpenTime - Avg
# TotalTime
# TotalTime - Avg
*/
# OpenTime
# OpenTime - Avg
# TotalTime
# TotalTime - Avg
*/