﻿CREATE VIEW [dbo].[vwDimHashTag]
	AS 
	 
SELECT htag.[Id]				AS HashtagGUID
      ,htag.[trek_level1]			AS HashtagL1
      ,htag.[trek_level2]			AS HashtagL2
      ,htag.[trek_level3]			AS HashtagL3
      ,htag.[trek_level4]			AS HashtagL4
      ,htag.[trek_level5]			AS HashtagL5
      ,htag.[trek_name]				AS HashtagFull
      ,stts.[LocalizedLabel]		        AS HashtagStatus
  FROM [dbo].[trek_hashtag] AS htag
	 INNER JOIN [dbo].StatusMetadata AS stts
		ON htag.statuscode = stts.[status]
		AND [LocalizedLabelLanguageCode] = '1033'
		AND EntityName= 'trek_hashtag'
--Note: In the cube, hashtags will have a hierarchical structure. Level 1 is the top level, and level 5 is the lowest. 
--It is acceptable that some of the lower level values are NULL, as the hashtag tree may not be that deep.